<?php

include('tpl_start.php');
?>

<div class="col-md-4 bg-secondary offset-md-4 login">
<form action="page.php">
<h4>Se connecter</h4>
<div class="form-group">
    <label for="InputEmail">Email address</label>
        <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We\'ll never share your email with anyone else.</small>
</div>
<div class="form-group">
    <label for="Password">Password</label>
    <input type="password" class="form-control" id="Password" placeholder="Password">
</div>

    <button type="submit" class="btn btn-dark">Valider</button>
    </form>
 
    <a href="mdp_perdu.php" class="btn btn-danger">Mot de passe oublié</a>
<?php
include('tpl_end.php');
?>